function cargarAjax() {
    const url = "https://jsonplaceholder.typicode.com/users";

    axios
    .get(url)
    .then((res)=>{
        mostrar(res.data)
    })
    .catch((err)=> {
        console.log("Surgió un error");
    })
    function mostrar(data) {
        const txtid = document.getElementById('txtID');
        let nombre = document.getElementById('txtNombre');
        let usuario = document.getElementById('txtUsuario');
        let email = document.getElementById('txtEmail');
        let calle = document.getElementById('txtCalle');
        let numero = document.getElementById('txtNumero');
        let ciudad = document.getElementById('txtCiudad');

        let posicion = buscarCampo(data, 0, data.length-1, txtid.value);

        if(posicion != -1) {
            nombre.value = data[posicion].name;
            usuario.value = data[posicion].username;
            email.value = data[posicion].email;
            calle.value = data[posicion].address.street;
            numero.value = data[posicion].phone;
            ciudad.value = data[posicion].address.city;
        }
        else {
            alert('Elemento no encontrado');
        }
    }
}

/* Implementación del algoritmo de Búsqueda Binaria */
function buscarCampo(data, inicio, final, campo) {
    let mitad = Math.round((inicio + final)/2);

    if(inicio == final && campo != data[mitad].id) {
        return -1;
    }
    else {
        if(campo == data[mitad].id)
            return mitad;
        else if(campo < data[mitad].id)
            return buscarCampo(data, inicio, mitad-1, campo);
        else
            return buscarCampo(data, mitad+1, final, campo);
    }
}

const buscar = document.getElementById('btnBuscar');
buscar.addEventListener('click', function() {
    cargarAjax();
});